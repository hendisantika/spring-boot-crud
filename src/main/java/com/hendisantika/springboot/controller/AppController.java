package com.hendisantika.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
//@RequestMapping
public class AppController {
    @GetMapping("/")
    public String home(Model modal) {
        modal.addAttribute("title", "CRUD Example");
        return "index";
    }


    @GetMapping("/partials/{page}")
    public String partialHandler(@PathVariable("page") final String page) {
        return page;
    }

}
